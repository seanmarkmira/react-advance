import React, { useState } from "react";
// short-circuit evaluation
// The utilization and reaction of using || and && in React

// ternary operator
// The utilization and reaction of using ? in React

// Why are we studying short-circuit and ternary:
//  In React, you can't use if statements in a return JSX, but can use if statements to return something (see 1-multiple-returns.js)

const ShortCircuit = () => {
  const [text, setText] = useState("");
  const [isError, setIsError] = useState(false);

  //In the firstValue utiliting ||, if one is true, return that text
  // const firstValue = text || "hello world";
  //In the secondValue utilizing &&, if text is true assign hello world
  // const secondValue = text && "hello world";

  return (
    <>
      {/* <h1>{firstValue}</h1> */}
      {/* <h1>value: {secondValue}</h1> */}
      {/* {if(){console.log("Hello world")}} */}
      <h2>{text || "sean mira"}</h2>
      <button className="btn" onClick={() => setIsError(!isError)}>
        toggle error
      </button>
      {isError && <h1>"Error..."</h1>}
      {isError ? (
        <p>Ternary shows TRUE (isError True)</p>
      ) : (
        <p>Ternary shows FALSE (isError False)</p>
      )}
    </>
  );
};

export default ShortCircuit;
