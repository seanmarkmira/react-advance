import React, { useState, useEffect } from "react";
// by default runs after every re-render
// cleanup function
// second parameter
const UseEffectBasics = () => {
  const [value, setValue] = useState(0);
  //useEffect is used when we want to do something outside of the component or every rerender. Common usecases are: Side Effect once something is triggered, data fetching, listening to events, signing in a subscription, and so on.
  useEffect(() => {
    console.log("Call useEffect");
    //Remember: You cannot use useEffect or any Hooks in conditionals like within an if statement and so on. If you need to use a condition, use the condition inside the hooks to make it work.
    if (value >= 1) {
      document.title = `New Message(${value})`;
    }
    //A blank array [] after the useEffect means that use this useEffect only in the first render. (This is called `dependency`)
    //By adding value as the parameter, it means that in every change in that value, we include the useEffect afterwards
  }, [value]);

  //You can add multiple useEffect in your page.
  useEffect(() => {
    console.log("Initial Render only");
  }, []);

  console.log("render component");

  return (
    <>
      <h1>{value}</h1>
      {/* Set value will re-render the page, the idea of useEffect is that everytime there is a re-render, it will be called */}
      <button className="btn" onClick={() => setValue(value + 1)}>
        Click Me
      </button>
    </>
  );
};

export default UseEffectBasics;
