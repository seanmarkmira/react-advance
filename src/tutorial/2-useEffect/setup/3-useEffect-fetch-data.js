import React, { useState, useEffect } from "react";

const url = "https://api.github.com/users";

// second arguement

const UseEffectFetchData = () => {
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    const response = await fetch(url);
    const users = await response.json();
    // The code below is essentially correct, we are assigning the data through setUsers, the problem here is the logic. Remember, useEffect is triggered in every rerender. setUsers triggers a rerender, which calls again useEffect, which uses again setUsers. This will trigger an inifinite loop in your React.
    // To remedy this problem of infinite loop, put dependency on your useEffect []. Which means, render only once.
    setUsers(users);
    // console.log(users);
  };

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <>
      <h3>fetch data</h3>
      <ul className="users">
        {users.map((user) => {
          const { id, login, avatar_url, html_url } = user;
          console.log(html_url);
          return (
            <li key={id}>
              <img src={avatar_url} alt={login} />
              <div>
                <h4>{login}</h4>
                <a href={html_url}>Profile</a>
              </div>
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default UseEffectFetchData;
