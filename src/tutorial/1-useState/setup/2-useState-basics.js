import React, { useState } from "react";
// Rules of HOOKS:
// use
// component name must be uppercase (useStateBasics will not work)
// must be in the function/component body (Must be in another function)
// cannot call conditionally (You cannot use the hooks in an conditions like if conditions)

const UseStateBasics = () => {
  //console.log(useState("Hello World"));
  // const value = useState(1)[0];
  // const handler = useState(1)[1];
  // console.log(value, handler);

  const handleClick = () => {
    if (text === "random title") {
      setText("Change succesful");
    } else {
      setText("random title");
    }
  };

  const [text, setText] = useState("random title");
  return (
    <React.Fragment>
      <h2>{text}</h2>
      <button className="btn" onClick={handleClick}>
        Change Title
      </button>
    </React.Fragment>
  );
};

export default UseStateBasics;
