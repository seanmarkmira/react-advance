import React from "react";
import { data } from "../../../data";

const UseStateArray = () => {
  const [people, setPeople] = React.useState(data);

  const removeItem = (id) => {
    //Filter the array. The condition here is that return the new array with !== to the id that we passed
    let newPeople = people.filter((person) => person.id !== id);
    console.log(newPeople);
    setPeople(newPeople);
  };

  return (
    <>
      {people.map((person) => {
        const { id, name } = person;
        return (
          <div key={id} className="item">
            <h4>{name}</h4>
            <button onClick={() => removeItem(id)}>Remove Specific</button>
          </div>
        );
      })}
      {/* You can call the 2nd arguement in the useState to change the value of the useState */}
      <button className="btn" onClick={() => setPeople([])}>
        Delete All
      </button>
    </>
  );
};

export default UseStateArray;
