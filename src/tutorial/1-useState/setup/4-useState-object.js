import React, { useState } from "react";

const UseStateObject = () => {
  const [person, setPerson] = useState({
    name: "peter",
    age: 24,
    message: "random message",
  });

  const changeMessage = () => {
    //...person means, copy the old values.. and the second arguement changes a specific key in the object
    //Object manipulation
    setPerson({ ...person, message: "Object Example" });
    // This is just an example of utilizing setMesage approach
    // setMessage("hello world");
  };

  // This is just an example of utilizing setMesage approach
  const [name, setName] = useState("peter");
  const [age, setAge] = useState(25);
  const [message, setMessage] = useState("random message");

  return (
    //Object manipulation
    <>
      <h3>{person.name}</h3>
      <h3>{person.age}</h3>
      <h3>{person.message}</h3>
      <button className="btn" onClick={changeMessage}>
        Change Message
      </button>
    </>
    // This is just an example of utilizing setMesage approach
    // <>
    //   <h3>{name}</h3>
    //   <h3>{age}</h3>
    //   <h3>{message}</h3>
    //   <button className="btn" onClick={changeMessage}>
    //     Change Message
    //   </button>
    // </>
  );
};

export default UseStateObject;
