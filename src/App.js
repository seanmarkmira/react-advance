import React from "react";
import HandsOn from "./tutorial/4-forms/setup/2-multiple-inputs";
// import Final from "./tutorial/2-useEffect/setup/3-fetch-data;

function App() {
  return (
    <div className="container">
      <HandsOn />
      {/* <h2>Advanced React Tutorial</h2> */}
    </div>
  );
}

export default App;
